#include <algorithm>
#include <boost/functional/hash.hpp>
#include <iostream>
#include <numeric>
#include <random>
#include <string>
#include <tuple>
#include <unordered_set>
#include <vector>

#include "catch.hpp"
#include "utils.hpp"

using namespace std;
using namespace Utils;

TEST_CASE("hashed containers")
{
    unordered_set<int> uset_int = { 1, 4, 7, 2435, 6, 665 };

    print(uset_int, "uset_int");

    uset_int.insert(42);

    REQUIRE(uset_int.find(42) != uset_int.end());
}

template <typename HashedContainer>
void print_unordered(const HashedContainer& container, string_view prefix)
{
    cout << prefix << ": "
         << "size: " << container.size()
         << "; bucket_count: " << container.bucket_count()
         << "; max_load_factor: " << container.max_load_factor()
         << "; load_factor: " << container.load_factor()
         << "\n";
}

TEST_CASE("unordered container api")
{
    random_device rd;
    mt19937_64 rnd_gen{ rd() };
    uniform_int_distribution<> distr{ 0, 100 };

    unordered_set<int> uset;
    uset.max_load_factor(2.0);
    print_unordered(uset, "uset");

    for (int i = 0; i < 64; ++i) {
        auto item = distr(rnd_gen);

        cout << item << ": " << uset.insert(item).second << endl;
        print_unordered(uset, "uset");
    }

    cout << "\nDetailed stat:\n";

    for (size_t i = 0; i < uset.bucket_count(); ++i) {
        cout << "Bucket " << i << ": [ ";
        for (auto it = uset.begin(i); it != uset.end(i); ++it) {
            cout << *it << " ";
        }
        cout << "]\n";
    }

    cout << "\n";

    uset.rehash(200);

    print_unordered(uset, "uset after rehash");

    uset.rehash(54);

    print_unordered(uset, "uset after rehash");
}

TEST_CASE("hashing")
{
    std::hash<int> hasher{};
    cout << "hash for 42: " << hasher(42) << endl;

    std::hash<std::string> str_hasher{};
    cout << "hash for text: " << str_hasher("text") << endl;
}

struct Person {
    string fname;
    string lname;
    uint8_t age;

    auto tied() const
    {
        return tie(fname, lname, age);
    }

    bool operator==(const Person& p) const
    {
        return tied() == p.tied();
    }
};

template <typename... Args>
size_t combine_hash(const Args&... args)
{
    size_t seed = 0;
    (boost::hash_combine(seed, args), ...);
    return seed;
}

//template <typename Tpl, size_t... Is>
//size_t hash_for_tuple_impl(const Tpl& tpl, std::index_sequence<Is...>)
//{
//    return combine_hash(std::get<Is>(tpl)...);
//}

//template <typename... Ts>
//size_t hash_for_tuple(const tuple<Ts...>& tpl)
//{
//    return hash_for_tuple_impl(tpl, std::make_index_sequence<sizeof...(Ts)>());
//}

namespace std {
template <>
struct hash<Person> {
    size_t operator()(const Person& p) const
    {
        //return hash_for_tuple(p.tied());

        auto combine_hash = [](const auto&... args) {
            size_t seed = 0;
            (boost::hash_combine(seed, args), ...);
            return seed;
        };

        std::apply(combine_hash, p.tied());
    }
};
}

struct HashPerson {
    size_t operator()(const Person& p) const
    {
        size_t seed = 0;
        boost::hash_combine(seed, p.fname);
        boost::hash_combine(seed, p.lname);
        boost::hash_combine(seed, p.age);

        return seed;
    }
};

TEST_CASE("custom hashed objects")
{
    unordered_set<Person, HashPerson> people = { Person{ "Jan", "Kowalski", 24 }, { "Adam", "Nowak", 56 } };

    REQUIRE(people.count(Person{ "Jan", "Kowalski", 24 }));
}
