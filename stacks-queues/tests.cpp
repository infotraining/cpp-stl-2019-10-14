#include <algorithm>
#include <bitset>
#include <iostream>
#include <numeric>
#include <queue>
#include <stack>
#include <string>
#include <vector>

#include "catch.hpp"
#include "utils.hpp"

using namespace std;

TEST_CASE("stack - LIFO")
{
    stack<string> words;

    words.push("one");
    words.push("two");
    words.push("three");

    while (!words.empty())
    {
        string item = words.top();
        words.pop();

        cout << item << "\n";
    }
}

TEST_CASE("queue - FIFO")
{
    cout << "\n";
    priority_queue<string, vector<string>, greater<>> q;

    q.push("one");
    q.push("two");
    q.push("three");

    while (!q.empty())
    {
        string item = q.top();
        q.pop();

        cout << item << "\n";
    }
}

TEST_CASE("bitset")
{
    bitset<16> bs1(32);
    bitset<16> bs2("010101010111");

    REQUIRE(bs2.test(0));
    REQUIRE(!bs2.flip(0).test(0));

    auto result = bs1 | (~bs2);

    cout << "result: " << result.to_string() << " - " << result.to_ulong() << endl;
}
