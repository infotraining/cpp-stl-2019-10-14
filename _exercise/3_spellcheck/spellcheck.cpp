#include <chrono>
#include <fstream>
#include <iostream>
#include <set>
#include <string>
#include <vector>
#include <regex>

using namespace std;

vector<std::string> regex_split(const std::string& s, const std::string& regex_str = R"(\s+)")
{
    std::vector<std::string> words;

    std::regex rgx(regex_str);

    std::sregex_token_iterator iter(s.begin(), s.end(), rgx, -1);
    std::sregex_token_iterator end;

    while (iter != end)
    {
        words.push_back(*iter);
        ++iter;
    }

    return words;
}

int main()
{
    // wczytaj zawartość pliku en.dict ("słownik języka angielskiego")    
    // sprawdź poprawość pisowni następującego zdania:    
    string input_text = "this is an exmple of very badd snetence";
    vector<string> words = regex_split(input_text);

    ifstream dict_file("en.dict");

    if (!dict_file)
        exit(1);

    set<string> dict(istream_iterator<string>(dict_file), istream_iterator<string> {});

    for (const auto& word : words) {
        if (!dict.count(word))
            cout << word << " ";
    }
    cout << "\n";
}
