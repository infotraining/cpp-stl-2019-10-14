#include <algorithm>
#include <deque>
#include <iostream>
#include <list>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"
#include "utils.hpp"

using namespace std;
using namespace Utils;

template <typename Container>
void print_stats(const Container& container, string_view prefix)
{
    cout << prefix << " - size: " << container.size() << "; capacity: " << container.capacity() << "\n";
}

TEST_CASE("vector")
{
    SECTION("default constructor")
    {
        vector<int> vec;

        print(vec, "vec");
        print_stats(vec, "vec");
    }

    SECTION("constructor with size")
    {
        vector<int> vec(10);
        print(vec, "vec");
        print_stats(vec, "vec");
    }

    SECTION("construct from initializer list")
    {
        vector vec = { 1, 2, 3, 4, 5, 6 };
        print(vec, "vec");
        print_stats(vec, "vec");
    }

    SECTION("construct from range")
    {
        array arr{ 1, 2, 3 };
        vector vec(begin(arr), end(arr));
        print(vec, "vec");
        print_stats(vec, "vec");
    }
}

TEST_CASE("push_backs")
{
    {
        vector<Gadget> gadgets;
        gadgets.reserve(64);

        for (int i = 0; i < 32; ++i) {
            gadgets.emplace_back("g");
            print_stats(gadgets, "gadgets after " + to_string(i + 1) + " pb");
        }

        cout << "\n";

        gadgets.insert(gadgets.begin(), Gadget("new"));
    }

    Gadget::print_stats();
}

TEST_CASE("invalidating iterators")
{
    vector vec = { 1, 4, 3, 4, 5, 4, 4, 4 };

    //    for (auto it = vec.begin(); it != vec.end();) {
    //        if (*it == 4)
    //            it = vec.erase(it);
    //        else
    //            ++it;
    //    }

    auto new_end = remove(begin(vec), end(vec), 4);
    vec.erase(new_end, vec.end());

    print(vec, "vec after remove");

    SECTION("erase - linear time - O(N)")
    {
        vec.erase(vec.begin());
    }

    SECTION("erase - constant time - O(1)")
    {
        auto efficient_erase = [](auto pos, auto& vec) {
            *pos = std::move(vec.back());
            vec.pop_back();
        };

        efficient_erase(vec.begin(), vec);
    }

    SECTION("shrink")
    {
        int* ptr_int = &vec[0];

        vec.clear();
        vec.shrink_to_fit();

        print_stats(vec, "vec after clear");
    }
}

TEST_CASE("vector<bool>")
{
    vector<bool> vec_bits = { 1, 0, 0, 1, 1, 0 };

    print(vec_bits, "vec_bits");

    vec_bits.flip();

    print(vec_bits, "vec_bits");

    //bool* ptr_bool = &vec_bits[0];

    for (auto&& bit : vec_bits)
        bit.flip();
}

TEST_CASE("deque")
{
    deque dq = { 1, 2, 3, 4 };

    int& ref_item = dq[2]; // getting a reference to an item

    dq.push_front(0);
    dq.push_back(5);

    REQUIRE(dq[3] == 3);
    REQUIRE(ref_item == 3); // after push_front reference is valid

    print(dq, "dq");
}

TEST_CASE("list")
{
    list<int> lst1 = { 1, 2, 3, 7, 234, 3, 56, 23543, 6 };
    print(lst1, "lst1");

    lst1.insert(std::next(lst1.begin(), 4), 665);
    print(lst1, "lst1");

    list lst2 = { 54, 12, 5, 235, 456, 665 };
    print(lst2, "lst2");

    lst1.sort();
    lst2.sort();

    print(lst1, "lst1");
    print(lst2, "lst2");
    lst1.merge(lst2);
    print(lst1, "lst1");
    print(lst2, "lst2");

    lst1.unique();
    print(lst1, "lst1");

    list lst3 = { 6, 6, 346, 23464, 2, 6, 7, 8 };
    print(lst3, "lst3");

    auto first = std::next(lst3.begin(), 2);
    auto last = std::next(lst3.begin(), 5);

    lst1.splice(lst1.begin(), lst3, first, last);

    print(lst1, "lst1 after splice");
    print(lst3, "lst3 after splice");

    lst1.remove_if([](int x) { return x % 2 == 0; });
    print(lst1, "odds");
}
