#include <algorithm>
#include <iostream>
#include <map>
#include <numeric>
#include <set>
#include <string>
#include <vector>

#include "catch.hpp"
#include "utils.hpp"

using namespace std;
using namespace Utils;

TEST_CASE("sets")
{
    set<int> set_int = { 1, 2, 7, 235, 3, 665, 3, 3, 42 };

    print(set_int, "set_int");

    if (auto pos = set_int.find(665); pos != set_int.end()) {
        cout << "Item found " << *pos << "\n";
    }

    set_int.erase(42);

    print(set_int, "set_int");

    if (auto [pos, was_inserted] = set_int.insert(665); was_inserted) {
        cout << *pos << " was inserted\n";
    } else {
        cout << *pos << " was already in a container\n";
    }

    set_int.emplace_hint(set_int.end(), 2018);
}

TEST_CASE("multiset")
{
    multiset<int> mset = { 1, 6, 2, 2, 3, 7, 7, 9, 42, 7, 665, 42 };

    print(mset, "mset");

    for (auto it = mset.lower_bound(7); it != mset.upper_bound(7); ++it) {
        cout << *it << " ";
    }
    cout << "\n";

    {
        auto [first_7, last_7] = mset.equal_range(7);
        mset.erase(first_7, last_7);
    }
    auto [first_7, last_7] = mset.equal_range(7);
    REQUIRE(first_7 == last_7);

    mset.emplace_hint(first_7, 7);
    print(mset, "mset");
}

bool ptr_less_compare(const std::unique_ptr<int>& a, const std::unique_ptr<int>& b)
{
    return *a < *b;
}

TEST_CASE("sorting functions")
{
    SECTION("as template parameter")
    {
        multiset<string, greater<>> mset = { "5", "76", "23", "42", "1", "234", "56", "665" };
        print(mset, "mset");

        if (auto pos = mset.find("abc"); pos != mset.end()) {
        }
    }

    SECTION("as function pointer")
    {
        multiset<std::unique_ptr<int>, bool (*)(const std::unique_ptr<int>& a, const std::unique_ptr<int>& b)> mset(ptr_less_compare);
        mset.insert(make_unique<int>(13));
        mset.insert(make_unique<int>(1));
        mset.insert(make_unique<int>(43));

        for (const auto& ptr : mset)
            cout << *ptr << " ";
        cout << "\n";
    }

    SECTION("as lambda")
    {
        auto cmp_ptr_less = [](const auto& a, const auto& b) { return *a < *b; };

        multiset<std::unique_ptr<int>, decltype(cmp_ptr_less)> mset(cmp_ptr_less);
        mset.insert(make_unique<int>(13));
        mset.insert(make_unique<int>(1));
        mset.insert(make_unique<int>(43));

        for (const auto& ptr : mset)
            cout << *ptr << " ";
        cout << "\n";
    }
}

TEST_CASE("maps")
{
    map<int, string> dict = { { 1, "one" }, { 3, "three" }, { 5, "five" } };

    print(dict, "dict");

    dict.insert(make_pair(2, "two"));
    dict.insert(pair{ 4, "four" });
    dict.emplace(6, "six");
    dict.insert_or_assign(6, "SIX");

    print(dict, "dict");

    for (const auto& [key, value] : dict) {
        cout << key << " - " << value << "\n";
    }

    cout << dict[1] << "\n";

    dict[0] = "ONE";

    cout << dict[7] << endl;

    print(dict, "dict");

    multimap<int, string, greater<>> multi_dict(dict.begin(), dict.end());

    multi_dict.insert(pair{ 1, "jeden" });
    multi_dict.emplace(1, "uno");

    auto [first_1, last_1] = multi_dict.equal_range(1);
    for (auto it = first_1; it != last_1; ++it)
        cout << it->first << " - " << it->second << "\n";

    print(multi_dict, "multi_dict");

    map<int, string> src = { { 1, "one" }, { 3, "three" } };
    map<int, string> dst = { { 2, "two" }, { 3, "trzy" } };

    dst.merge(src);

    print(src, "src");
    print(dst, "dst");
}
