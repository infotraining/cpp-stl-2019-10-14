#include <algorithm>
#include <iostream>
#include <iterator>
#include <list>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"
#include "utils.hpp"

using namespace std;

TEST_CASE("iterators")
{
    vector vec = {1, 2, 3, 4};

    REQUIRE(*(vec.data() + 3) == 4);

    static_assert(is_same_v<iterator_traits<vector<int>::iterator>::iterator_category, random_access_iterator_tag>);
}

template <typename Iterator>
void advance_it(Iterator& it, size_t n)
{
    if constexpr (is_same_v<typename iterator_traits<Iterator>::iterator_category, random_access_iterator_tag>)
        it += n;
    else
    {
        for (size_t i = 0; i < n; ++i)
            ++it;
    }
}

TEST_CASE("advance_it")
{
    list lst = {1, 2, 3, 4, 5, 6};

    auto it = lst.begin();

    advance_it(it, 3);

    REQUIRE(*it == 4);
}

template <typename InputIterator, typename OutputIterator>
OutputIterator my_copy(InputIterator first, InputIterator last, OutputIterator out)
{
    for (auto it = first; it != last; ++it, ++out)
    {
        *out = *it;
    }

    return out;
}

TEST_CASE("categories of iterators")
{
    vector vec = {1, 2, 3, 4, 5};
    list<int> lst(vec.size());

    my_copy(begin(vec), end(vec), begin(lst));
}

TEST_CASE("insert iterator")
{
    vector vec = {1, 2, 3, 4};

    back_insert_iterator bit(vec);

    *bit = 5;
    ++bit;
    *bit = 6;
    ++bit;
    *bit = 7;
    ++bit;

    Utils::print(vec, "vec");

    list<int> lst;

    my_copy(vec.begin(), vec.end(), front_inserter(lst));

    Utils::print(lst, "lst");

    copy_if(lst.begin(), lst.end(), inserter(vec, vec.begin() + 2), [](int x) { return x % 2 == 0; });

    Utils::print(vec, "vec");
}

bool is_palindrome(const string& text)
{
    auto middle = next(text.begin(), text.size() / 2);

    string::const_iterator it = text.begin();
    string::const_reverse_iterator rit = text.rbegin();

    //    for (; it < rit.base(); ++it, ++rit)
    //        if (*it != *rit)
    //            return false;
    //    return true;

    return equal(text.begin(), middle, text.rbegin());
}

TEST_CASE("is_palindrome")
{
    REQUIRE(is_palindrome("potop"));
    REQUIRE(is_palindrome("abba"));
    REQUIRE_FALSE(is_palindrome("abbc"));
    REQUIRE_FALSE(is_palindrome("abca"));
}

TEST_CASE("reverser iterator")
{
    vector vec = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    list<int> lst;

    copy_if(vec.rbegin(), vec.rend(), back_inserter(lst), [](int x) { return x % 2 == 0; });

    Utils::print(lst, "lst");
}

TEST_CASE("stream iterators")
{
    istream_iterator<int> inp_it(cin);
    istream_iterator<int> end_it {};

    vector<int> vec(inp_it, end_it);

    copy(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "));
    cout << "\n";
}
