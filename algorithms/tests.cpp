#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>
#include <random>
#include <string>
#include <vector>

#include "catch.hpp"
#include "utils.hpp"

using namespace std;

inline void print_item(int x)
{
    cout << x << "\n";
}

struct Counter
{
    int value {};

    void operator()(int x)
    {
        value += x;
    }
};

namespace Explain
{
    template <typename InputIterator, typename F>
    F for_each(InputIterator first, InputIterator last, F f)
    {
        for (auto it = first; it != last; ++it)
            f(*it);

        return f;
    }
}

class Person
{
    string name_;

public:
    Person(string name)
        : name_ {move(name)}
    {
    }

    void print() const
    {
        cout << "Person(" << name_ << ")\n";
    }
};

TEST_CASE("function pointers vs functors")
{
    vector vec = {1, 2, 3, 4};

    Explain::for_each(vec.begin(), vec.end(), print_item);

    auto result = Explain::for_each(vec.begin(), vec.end(), Counter {});

    cout << "sum: " << result.value << "\n";

    Explain::for_each(vec.begin(), vec.end(), [](int x) { cout << x << " "; });
    cout << "\n";

    vector<int> squares(vec.size());
    transform(vec.begin(), vec.end(), vec.begin(), squares.begin(), multiplies<> {});
    Utils::print(squares, "squares");

    vector<Person> vips = {Person {"Jan"}, Person {"Adam"}, Person {"Ewa"}};
    for_each(vips.begin(), vips.end(), mem_fn(&Person::print));
    for_each(vips.begin(), vips.end(), [](const auto& p) { p.print(); });
}

TEST_CASE("binary search & others")
{
    vector<int> vec = {5, 235, 62, 346, 665, 42, 23546, 8};
    sort(vec.begin(), vec.end());

    REQUIRE(binary_search(vec.begin(), vec.end(), 42));

    auto [lb, ub] = equal_range(vec.begin(), vec.end(), 664);

    REQUIRE(lb == ub);

    vec.insert(ub, 664);

    Utils::print(vec, "vec");

    auto new_end = remove_if(vec.begin(), vec.end(), [](int x) { return x % 2 == 0; });
    vec.erase(new_end, vec.end());

    Utils::print(vec, "vec");
}

TEST_CASE("sorting")
{
    vector<int> vec(20);

    random_device rd;
    mt19937 rnd_gen(rd());
    uniform_int_distribution<> distr(1, 100);

    generate_n(vec.begin(), 20, [&distr, &rnd_gen] { return distr(rnd_gen); });

    Utils::print(vec, "vec");

    nth_element(vec.begin(), vec.begin() + 5, vec.end(), greater {});
    copy_n(vec.begin(), 5, ostream_iterator<int>(cout, " "));
    cout << "\n";

    partial_sort(vec.begin(), vec.begin() + 5, vec.end(), greater {});
    copy_n(vec.begin(), 5, ostream_iterator<int>(cout, " "));
    cout << "\n";
}
