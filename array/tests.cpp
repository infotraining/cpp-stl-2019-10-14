#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"
#include "utils.hpp"

using namespace std;
using namespace Utils;

namespace Description {
template <typename T, size_t N>
struct Array {
    using value_type = T;
    using reference = T&;
    using iterator = T*;

    T item[N];

    T& operator[](size_t index)
    {
        return item[index];
    }
};
}

std::array<int, 3> get_coords()
{
    return { 1, 2, 3 };
}

TEST_CASE("array")
{
    SECTION("is substitute for c-style array")
    {
        int tab1[10];
        print(tab1, "tab1");

        int tab2[3] = { 1, 2, 3 };
        print(tab2, "tab2");

        int tab3[10] = { 1, 2, 3 };
        print(tab3, "tab3");

        int tab4[10] = {};
        print(tab4, "tab4");

        REQUIRE(size(tab4) == 10);
    }

    SECTION("as better alternative")
    {
        std::array<int, 10> arr1;
        print(arr1, "arr1");

        static_assert(is_aggregate_v<decltype(arr1)>);

        std::array arr2 = { 1, 2, 3 };
        std::array<int, 10> arr3 = { 1, 2, 3 };
        std::array<int, 10> arr4 = {};
    }

    SECTION("can be returned from function")
    {
        std::array<int, 3> coords = get_coords();
        REQUIRE(coords == array{ 1, 2, 3 });

        auto [x, y, z] = get_coords();
        REQUIRE(x == 1);
        REQUIRE(z == 3);
    }

    array data = { 1, 2, 3 };

    SECTION("at()")
    {
        REQUIRE_THROWS_AS(data.at(5), std::out_of_range);
    }

    SECTION("")
    {
        data.fill(-1);
        REQUIRE(data.size() == 3);
    }

    SECTION("swap")
    {
        array other = { 5, 6, 7 };
        data.swap(other);

        REQUIRE(data == array{ 5, 6, 7 });
        REQUIRE(other == array{ 1, 2, 3 });
    }
}

namespace Legacy {
void use_tab(int* tab, size_t size)
{
    cout << "using tab: ";
    for (int* it = tab; it != tab + size; ++it) {
        cout << *it << " ";
    }
    cout << "\n";
}
}

TEST_CASE("using array in legacy code")
{
    array dataset = { 1, 2, 3 };
    Legacy::use_tab(dataset.data(), dataset.size());
}

TEST_CASE("tuple like interface")
{
    array arr = { 1, 2, 3 };

    REQUIRE(get<0>(arr) == 1);
    REQUIRE(get<1>(arr) == arr[1]);
    REQUIRE(get<2>(arr) == 3);
}

template <typename T, size_t N, typename F, size_t... Is>
constexpr auto transform_impl(const std::array<T, N>& arr, F f, std::index_sequence<Is...>)
{
    return array{ f(get<Is>(arr))... };
}

template <typename T, size_t N, typename F>
constexpr auto transform_array(const std::array<T, N>& arr, F f)
{
    return transform_impl(arr, f, std::make_index_sequence<N>());
}

TEST_CASE("array can be used as constexpr")
{
    constexpr array data = { 1, 2, 3 };

    constexpr array transformed_data = transform_array(data, [](int x) { return x * x; });
}
